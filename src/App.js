import React, { Component } from 'react';
import Contacts from './Contacts/Contacts';

class App extends Component {
  render() {
    return (
      <main className="ui main text container">
        <Contacts />
      </main>
    );
  }
}

export default App;
