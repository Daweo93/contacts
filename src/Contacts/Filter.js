import React from 'react';

const Filter = ({ onFiltered }) => {
  return (
    <div className="ui form">
      <div className="field input">
        <input
          type="text"
          placeholder="Filter by name"
          onInput={e => onFiltered(e)}
        />
      </div>
    </div>
  );
};

export default Filter;
