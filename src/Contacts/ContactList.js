import React from 'react';
import ContactItem from './ContactItem';

const ContactList = ({ persons, userSelected }) => {
  if (!persons.length) {
    return <div>No item found!</div>;
  }

  return (
    <div className="ui relaxed divided list selection">
      {persons.map((person, index) => (
        <ContactItem key={index} person={person} userSelected={userSelected} />
      ))}
    </div>
  );
};

export default ContactList;
