import * as React from 'react';
import AppHeader from './AppHeader';
import ContactList from './ContactList';
import persons from './persons';
import getFilteredUsersForText from './getFilteredUsersForText';
import ContactItem from './ContactItem';
import Filter from './Filter';

class Contacts extends React.Component {
  constructor() {
    super();

    this.state = {
      filteredPersons: persons,
      selectedUser: null
    };
  }

  static defaultProps = { getFilteredUsersForText };

  filterList = event => {
    let text = event.target.value;
    this.props.getFilteredUsersForText(persons, text).then(filteredPersons => {
      this.setState({
        filteredPersons
      });
    });
  };

  onUserSelected = selectedUser => {
    this.setState({ selectedUser });
  };

  renderSelectedItem = selectedUser => {
    return (
      <React.Fragment>
        <div className="ui divider"> </div>
        <div className="ui relaxed divided list selection">
          <ContactItem person={selectedUser} />
        </div>
      </React.Fragment>
    );
  };

  render() {
    return (
      <div>
        <AppHeader />
        <Filter onFiltered={this.filterList} />

        <ContactList
          persons={this.state.filteredPersons}
          userSelected={this.onUserSelected}
        />

        {this.state.selectedUser &&
          this.renderSelectedItem(this.state.selectedUser)}
      </div>
    );
  }
}

export default Contacts;
