import React from 'react';
import { shallow } from 'enzyme';
import ContactItem from '../ContactItem';
import Avatar from '../Avatar';
import persons from '../persons';

describe('ContactItem', function() {
  const person = persons[0];
  let component;

  beforeEach(function() {
    component = shallow(<ContactItem person={person} />);
  });

  it('should contains Avatar component', function() {
    expect(component.containsMatchingElement(<Avatar />)).toEqual(true);
  });

  it(`should have name ${person.login}`, function() {
    expect(component.find('.header').text()).toEqual(person.login);
  });

  it(`should have description ${person.description}`, function() {
    expect(component.find('.description').text()).toEqual(person.department);
  });

  it('should call provided function userSelected', () => {
    const userSelectedStub = jest.fn();

    component = shallow(
      <ContactItem person={persons[0]} userSelected={userSelectedStub} />
    );
    component.find('.item').simulate('click');
    expect(userSelectedStub).toHaveBeenCalledTimes(1);
    expect(userSelectedStub).toHaveBeenCalledWith(person);
  });
});
