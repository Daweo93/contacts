import React from 'react';
import { shallow } from 'enzyme';
import Avatar from '../Avatar';

describe('Avatar', () => {
  let component;

  beforeEach(() => {
    component = shallow(<Avatar />);
  });

  it('should contain img tag', () => {
    expect(component.containsMatchingElement(<img />)).toEqual(true);
  });

  it('should have correct url with provided avatar name', function() {
    component = shallow(<Avatar avatar="typeofweb1" />);
    expect(component.find('img').prop('src')).toEqual(
      'https://api.adorable.io/avatars/55/typeofweb1.png'
    );
  });

  it('should have correct url without provided avatar name', function() {
    component = shallow(<Avatar avatar="" />);
    expect(component.find('img').prop('src')).toEqual(
      'https://api.adorable.io/avatars/55/default.png'
    );
  });
});
