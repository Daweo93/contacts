import React from 'react';
import { shallow } from 'enzyme';
import Filter from '../Filter';

describe('Filter', () => {
  let cmp;
  const onFilteredStub = jest.fn();

  beforeEach(() => {
    cmp = shallow(<Filter onFiltered={onFilteredStub} />);
  });

  it('should render without crashing', () => {
    expect(cmp);
  });

  it('should contains input element', () => {
    expect(cmp.find('input').length).toEqual(1);
  });

  it('should get onFiltered as a prop and call it', () => {
    cmp.find('input').simulate('input', { target: { value: 'M' } });
    expect(onFilteredStub).toHaveBeenCalledTimes(1);
    expect(onFilteredStub).toHaveBeenCalledWith({ target: { value: 'M' } });
  });
});
