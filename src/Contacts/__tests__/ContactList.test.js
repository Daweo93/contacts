import React from 'react';
import { shallow, mount } from 'enzyme';
import ContactList from '../ContactList';
import persons from '../persons';

describe('Contact List', function() {
  it('show message when there are no users', function() {
    const component = shallow(<ContactList persons={[]} />);
    expect(component.text()).toContain('No item found!');
  });

  it("shouldn't show message when there are users", function() {
    const component = shallow(<ContactList persons={persons} />);
    expect(component.text()).not.toContain('No item found!');
  });

  it('should render four persons on list', function() {
    const component = mount(<ContactList persons={persons} />);
    expect(component.find('.item').length).toEqual(persons.length);
  });

  it('should contain userSelected and persons as a prop', () => {
    const userSelectedFn = jest.fn();

    const component = mount(
      <ContactList persons={persons} userSelected={userSelectedFn} />
    );

    expect(component.prop('persons')).toEqual(persons);
    expect(component.prop('userSelected')).toEqual(userSelectedFn);
  });
});
