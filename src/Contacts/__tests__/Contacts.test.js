import React from 'react';
import { shallow } from 'enzyme';
import persons from '../persons';
import sinon from 'sinon';

import Contacts from '../Contacts';
import ContactList from '../ContactList';
import getFilteredUsersForText from '../getFilteredUsersForText';

describe('Contacts', () => {
  it('should contains ContactList', () => {
    const component = shallow(<Contacts />);
    expect(component.containsMatchingElement(<ContactList />)).toEqual(true);
  });

  it('should contains filter form element', () => {
    const component = shallow(<Contacts />);
    expect(component.find('Filter').length).toEqual(1);
  });

  it('should contain persons as prop', () => {
    const component = shallow(<Contacts />);
    expect(component.find('ContactList').prop('persons')).toEqual(persons);
  });

  it('should update state by selected user', () => {
    const component = shallow(<Contacts />);
    component.instance().onUserSelected(persons[0]);
    expect(component.state('selectedUser')).toEqual(persons[0]);
  });

  it('should render selected item', () => {
    const component = shallow(<Contacts />);
    component.setState({ selectedUser: persons[0] });
    expect(component.find('ContactItem').length).toEqual(1);
    expect(component.find('ContactItem').prop('person')).toEqual(persons[0]);
  });

  describe('Contacts on input change', async () => {
    let component;
    const persons = [
      {
        avatar: 'typeofweb3',
        login: 'Kamil',
        department: 'Back-end Developer'
      },
      {
        login: 'Kamil',
        department: 'Back-end Developer'
      }
    ];
    const event = {
      target: {
        value: 'M'
      }
    };

    it('should call stubbed method', () => {
      const getFilteredUsersForText = sinon.stub().resolves();
      const component = shallow(
        <Contacts getFilteredUsersForText={getFilteredUsersForText} />
      );
      component.instance().filterList(event);
      expect(getFilteredUsersForText.callCount).toEqual(1);
    });

    it('should return filtered list', async () => {
      const stubFiltered = sinon.stub().resolves(persons);
      component = await shallow(
        <Contacts getFilteredUsersForText={stubFiltered} />
      );

      await component.instance().filterList(event);
      await expect(component.state('filteredPersons')).toEqual(persons);

      await component.update();

      expect(component.find('ContactList').prop('persons').length).toEqual(2);
    });

    it('should return not filtered list', async () => {
      component = await shallow(
        <Contacts getFilteredUsersForText={getFilteredUsersForText} />
      );

      component.instance().filterList({ target: { value: '' } });

      await component.update();

      expect(component.find('ContactList').prop('persons').length).toEqual(4);
    });
  });
});
