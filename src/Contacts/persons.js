export default [
  {
    avatar: 'typeofweb1',
    login: 'Lena',
    department: 'Javascript Developer'
  },
  {
    avatar: 'typeofweb2',
    login: 'Dawid',
    department: 'Front-end Developer'
  },
  {
    avatar: 'typeofweb3',
    login: 'Kamil',
    department: 'Back-end Developer'
  },
  {
    login: 'Kamil',
    department: 'Back-end Developer'
  }
];
