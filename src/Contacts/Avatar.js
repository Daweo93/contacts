import React from 'react';

const Avatar = ({ avatar }) => {
  const avatarImg = avatar ? avatar : 'default';
  const imgUrl = `https://api.adorable.io/avatars/55/${avatarImg}.png`;

  return <img src={imgUrl} className="ui mini rounded image" alt="" />;
};

export default Avatar;