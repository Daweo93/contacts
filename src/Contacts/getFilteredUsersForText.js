export default function getFilteredUsersForText(persons, text) {
  return new Promise(resolve => {
    let lastRequest;

    if (lastRequest) {
      clearTimeout(lastRequest);
    }

    const time = (Math.random() + 1) * 250;
    lastRequest = setTimeout(() => {
      const filteredUsers = persons.filter(({ login }) =>
        login.toLowerCase().includes(text.toLowerCase())
      );
      resolve(filteredUsers);
    }, time);
  });
}
