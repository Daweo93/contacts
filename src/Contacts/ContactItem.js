import React from 'react';
import Avatar from './Avatar';

const ContactItem = ({ person, userSelected }) => {
  const { avatar, login, department } = person;

  return (
    <div
      className="item"
      onClick={() => (userSelected ? userSelected(person) : null)}
    >
      <Avatar avatar={avatar} />
      <div className="content">
        <h4 className="header">{login}</h4>
        <div className="description">{department}</div>
      </div>
    </div>
  );
};

export default ContactItem;
