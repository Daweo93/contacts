import React from 'react';

export default () => {
  return (
    <header className="ui menu">
      <nav className="ui container">
        <div className="header item">Lista kontaktów</div>
        <div className="header item">
          <button className="ui button">Dodaj</button>
        </div>
      </nav>
    </header>
  );
};
